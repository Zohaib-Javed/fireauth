import React,{useState} from 'react';
import Auth from '@react-native-firebase/auth';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Button,
  KeyboardAvoidingView,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

const SignupScreen= ({navigation}) => {
  const [email,setEmail]=useState("");
  const [password,setPassword]=useState("");
  const [conPassword,setConPassword]=useState("");
  const [error,setError]=useState("");
  const [loading,setLoading]=useState(false);
  const validation=()=>{
    if(email.length==0){
		console.log("Enter email");
		setError("Enter email");
		return false;
    } 
    else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
		console.log("Enter a valid Email Id");
		setError("Enter a valid Email Id");
		return false;
	}else if(password.length==0){
		console.log("Enter pass");
		setError("Enter Password");
		return false;
	}
	
    else if(password!==conPassword){
        setError("Password and Confirm Password are not Matched");
        return false;
      }
      return true;
  }
  const signUp=async()=>{
    setLoading(true);
      if(validation()){
        try{
            const signup=await Auth().createUserWithEmailAndPassword(email,password);
            if(error!="") setError("");
            console.log("signup");
            setLoading(false);
            navigation.navigate("Login");
          }
          catch(err){
            console.log(err.code);
            if(err.code==="auth/invalid-email"){
                console.log(err.code);
                setError("Enter a valid Email Id");
            }
            else if(err.code==="auth/weak-password"){
                console.log(err.code);
                setError("Password should contain > 5 characters");
            }
            else if(err.code==="auth/email-already-in-use"){
                console.log(err.code);
                setError("Email Id already exist");
            }
            setLoading(false);
        } 
      }
      else{
        setLoading(false);
        return;
      }
     
  }
  return (
    
    <KeyboardAvoidingView style={styles.container}>
        <View style={styles.loading}>
            <Spinner visible={loading} textContent="Wait...."  textStyle={{color:"green"}} size="large" color="green"></Spinner>
        </View>
      <View style={styles.headingView}>  
        <Text style={styles.heading}>Sign Up</Text>
      </View>
      <View style={styles.errorView}>
        <Text style={styles.errorText}>{error}</Text>
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.textInput}  
          keyboardType='email-address'
          placeholder="Email"
          autoCapitalize='none'
          onChangeText={email=>setEmail(email)}
          value={email}
          >
        </TextInput>

        <TextInput
          style={styles.textInput}
          placeholder="Password"
          autoCapitalize='none'
          secureTextEntry={true}
          onChangeText={password=>setPassword(password)}
          value={password}
          >
        </TextInput>
        <TextInput
          style={styles.textInput}
          placeholder="Confirm Password"
          autoCapitalize='none'
          secureTextEntry={true}
          onChangeText={password=>setConPassword(password)}
          value={conPassword}
          >
        </TextInput>
      </View>
      <View style={styles.buttonView}>
        <View style={styles.button}>
            <Button title='Login' onPress={()=>{navigation.navigate('Login')}}></Button>
        </View>
        <View style={styles.button}>
            <Button title='Sign Up' onPress={()=>{signUp()}}></Button>
        </View>
      </View>
     
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  textInput:{
    borderBottomColor:"gray",
    borderBottomWidth:1,
    marginHorizontal:30,
    paddingHorizontal:10,
  },  
  inputView:{
    width:"100%",
  },  
  heading:{
    fontSize:22,
    fontWeight:"bold",
  },
  headingView:{
    paddingBottom:20,
  },
  buttonView:{
    marginHorizontal:20,
    flexDirection:'row',
    justifyContent:'space-between',
  },  
  button:{
    width:"40%",
    margin:20,
  },
  errorView:{

  },
  errorText:{
    color:'red'
  },
  loading:{
	position:'absolute',
	width:'100%',
	height:'100%',
  }
});

export default SignupScreen;
