import React,{useState} from 'react';
import Auth from '@react-native-firebase/auth';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Button,
  
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

const LoginScreen= ({navigation}) => {
  const [email,setEmail]=useState("");
  const [password,setPassword]=useState("");
  const [error,setError]=useState("");
  const [loading,setLoading]=useState(false);

  const validation=()=>{
	if(email.length==0){
		console.log("Enter email");
		setError("Enter email");
		return;
	}
	else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
		console.log("Enter a valid Email Id");
		setError("Enter a valid Email Id");
		return;
	} 
	else if(password.length==0){
		console.log("Enter pass");
		setError("Enter Password");
		return;
	}
	
	else{
		login();
	}
		
  }
  const login=async()=>{
    setLoading(true);
    try{
    	const login=await Auth().signInWithEmailAndPassword(email,password);
    	if(error!="") setError("");
    	console.log("login");
		setLoading(false);
		navigation.navigate("Home");
    }
    catch(err){
		if(err.code==="auth/invalid-email"){
			console.log(err.code);
			setError("Enter a valid Email Id");
		}
		if(err.code==="auth/user-not-found"){
			console.log(err.code);
			setError("User is not found with this email ID");
		}
		else if(err.code==="auth/wrong-password"){
			console.log(err.code);
			setError("Password is incorrect");
		}
		setLoading(false);
    }
  
  }
  return (
  <View style={styles.container}>
    <View style={styles.loading}>
      <Spinner visible={loading} textContent="Wait...."  textStyle={{color:"green"}} size="large" color="green"></Spinner>
    </View>
    
    <View style={styles.headingView}>  
    	<Text style={styles.heading}>Login</Text>
    </View>
	<View style={styles.errorView}>
    	<Text style={styles.errorText}>{error}</Text>
    </View>
    <View style={styles.inputView} >
    <TextInput
      style={styles.textInput}
      keyboardType='email-address'
      placeholder="Email"
      autoCapitalize='none'
      onChangeText={email=>setEmail(email)}
      value={email}
      >
    </TextInput>

    <TextInput
      style={styles.textInput}
      placeholder="Password"
      autoCapitalize='none'
      secureTextEntry={true}
      onChangeText={password=>setPassword(password)}
      value={password}
      >
    </TextInput>
    </View>
    <View style={styles.buttonView}>
    <View style={styles.button}>
      <Button title='Login' onPress={()=>{validation()}}></Button>
    </View>
    <View style={styles.button}>
      <Button title='Sign Up' onPress={()=>{navigation.navigate('SignUp')}}></Button>
    </View>
    </View>
    
  </View>
  );
};

const styles = StyleSheet.create({
  container: {
	flex: 1,
	justifyContent: 'center',
	alignItems: 'center',
	backgroundColor: '#F5FCFF',
  },
  textInput:{
	borderBottomColor:"gray",
	borderBottomWidth:1,
	marginHorizontal:30,
	paddingHorizontal:10,
  },  
  inputView:{
  	width:"100%",
  },  
  heading:{
	fontSize:22,
	fontWeight:"bold",
  },
  headingView:{
  	paddingBottom:20,
  },
  buttonView:{
	marginHorizontal:20,
	flexDirection:'row',
	justifyContent:'space-between',
  },  
  button:{
	width:"40%",
	margin:20,
  },
  errorView:{

  },
  errorText:{
  	color:'red'
  },
  loading:{
	position:'absolute',
	width:'100%',
	height:'100%',
  }
});

export default LoginScreen;
